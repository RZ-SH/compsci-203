module Analysis

using Plots, LaTeXStrings, Latexify

const imgdir = "../slides/images/analysis/"

function example_welcome()
    x(n) = (n+1)/(2*n^2)
    label=L"\frac{n+1}{2 n^2}" 
    filename = "convergent-sequnce-1.pdf"
    return drawseq(x, label, filename, 0:10)
end

function example_2_1_8()
    x(n) = (n^2+1)/(n^2+n)
    label=L"\frac{n^2+1}{n^2+n}" 
    filename = "bai-2-1-8.pdf"
    return drawseq(x, label, filename)
end

function example_2_1_12()
    x(n) = sum([1/i for i in 1:n])
    label=L"1+\frac{1}{2}+\cdots\frac{1}{n}" 
    filename = "bai-2-1-12.pdf"
    return drawseq(x, label, filename)
end

function monotone_1()
    x(n) = n
    label=L"n"
    filename = "montone_1.pdf"
    return drawseq(x, label, filename)
end

function monotone_2()
    x(n) = 1-1/n
    label=L"1-\frac{1}{n}"
    filename = "montone_2.pdf"
    return drawseq(x, label, filename, 1:10:101)
end

function drawseq(x, label, filename, xs = 1:100:1001)
    ys = x.(xs)
    plt = scatter(xs, 
            ys, 
            label=label, 
            legend=:bottomright,
            xlabel = L"n")
    path = joinpath(imgdir, filename)
    savefig(plt, path)
    return path
end

end
