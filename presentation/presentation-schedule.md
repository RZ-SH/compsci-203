# Presentation Schedule

The presentation will be held in two sessions:

* Remote students  --- Thursday October 13th, 19:00-20:00
* Onsite students -- Friday October 14th, 13:00-15:00

You only need to attend the session in which you present.

The onsite session will be publicized to the whole university.
🥐 and 🧉 will be provided.

Please leave a comment regarding the topic which you choose.
We don't want to listen to the same topic twice.

See #47 for detailed requirements.

🏆 The best onsite presentation will win a book as a prize.
