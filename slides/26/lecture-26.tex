\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\begin{document}

\maketitle

\section{\acs{bai} 2.1 Sequences and limits}

\subsection{2.1.1 Monotone sequences}

\begin{frame}
    \frametitle{Keeling Curve}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Mauna_Loa_CO2_monthly_mean_concentration.png}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Monotone sequences}

    \begin{block}{Definition 2.1.9}
        A sequence $\{x_n\}$ is \alert{monotone increasing} 
        if $x_n ≤ x_n+1$ for all $n ∈ \dsN$. 

        A sequence $\{x_n\}$ is \alert{monotone decreasing} if \blankshort{}.
        (You can fill the gap here, right? \laughing{})

        If a sequence is either \emph{monotone increasing} or \emph{monotone
        decreasing}, we can simply say the sequence is \alert{monotone}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Examples}

    Monotone \emoji{up-arrow} sequences include ---
    \begin{itemize}
        \item $1, 2, 3, 4, \dots$
        \item $1, 2, 4, 8, \dots$
        \item $\{1-1/n\}$
    \end{itemize}
    Monotone \emoji{down-arrow} sequences include ---
    \begin{itemize}
        \item $-1, -2, -3, -4, \dots$
        \item $-1, -2, -4, -8, \dots$
        \item $\{1+1/n\}$
    \end{itemize}

    \cake{} Give an example sequence which is not monotone.
\end{frame}

\begin{frame}
    \frametitle{When is a monotone sequence convergent}

    Which of the two monotone \emoji{up-arrow} sequences is convergent?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\textwidth]{montone_1.pdf}
        \includegraphics<2>[width=0.8\textwidth]{montone_2.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Limits of monotone sequences --- Proposition 2.1.10}

    A monotone sequence ${x_n}$ is bounded if and only if it is convergent.

    Furthermore, if ${x_n}$ is monotone \emoji{up-arrow} and bounded, then
    \begin{equation*}
        \lim_{n \to \infty} x_n = \sup\{x_n : n ∈ \dsN\}.
    \end{equation*}

    \pause{}

    If ${x_n}$ is monotone \emoji{down-arrow} and bounded, then
    \begin{equation*}
        \lim_{n \to \infty} x_n = \inf\{x_n : n ∈ \dsN\}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2.1.11}
    
    Is the sequence $\left\{\frac{1}{\sqrt{n}}\right\}$ convergent?
    What is the limit?
\end{frame}

\begin{frame}
    \frametitle{Example 2.1.12}
    
    Is the sequence $\left\{1 + \frac{1}{2} + \cdots + \frac{1}{n}\right\}$ convergent?
    What is the limit?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{bai-2-1-12.pdf}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Proposition 2.1.13}

    Let $S ⊂ \dsR$ be a nonempty bounded set.

    Then there exist monotone sequences $\{x_n\}$ 
    and 
    $\{y_n\}$ such that $x_n, y_n \in S$ and
    \begin{equation*}
        \sup S = \lim_{n\to \infty} x_n
    \end{equation*}
    and
    \begin{equation*}
        \inf S = \lim_{n\to \infty} y_n
    \end{equation*}

    \cool{} Proof left as exercise.
\end{frame}

\subsection{2.1.3 Subsequences}

\begin{frame}
    \frametitle{Subsequences}

    \begin{block}{Definition 2.1.16}
        Let $\{x_n\}$ be a sequence.
        Let $\{n_i\}$ be a strictly increasing sequence of natural numbers, 
        The sequence $\{x_{n_i}\}$ is called a subsequence of $\{x_n\}$.
    \end{block}

    \cake{} Consider
    \begin{equation*}
        \{x_{n}\} = \left\{\frac{1}{n}\right\} = 
        \frac{1}{1},
        \frac{1}{2},
        \frac{1}{3},
        \frac{1}{4},
        \frac{1}{5},
        \frac{1}{6},
        \frac{1}{7},
        \frac{1}{8},
        \frac{1}{9},
        \frac{1}{10},
        \dots
    \end{equation*}
    Let $n_{i} = 2{i}$. What is
    \begin{equation*}
        \{x_{n_{i}}\} = \phantom{\left\{\frac{1}{2 n}\right\}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Convergence of subsequence}

    \begin{block}{Proposition 2.1.17}
        If $\{x_n\}$ is a convergent sequence,
        then every subsequence ${x_{n_i}}$ is also convergent,
        and
        \begin{equation*}
            \lim_{n \to \infty} x_{n}
            =
            \lim_{n \to \infty} x_{n_{i}}
            .
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Is it convergent?}

    \cake{} Can you think of a sequence which has a convergent sequence but itself is
    not convergent?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \begin{block}{Exercise 2.1.20}
        Let $\{x_n\}$ be a sequence and define a sequence 
        $\{y_n\}$ by $y_{2k}:= x_{k^2}$ 
        and $y_{2k−1} := x_k$ for all $k ∈ N$. 
        Prove that $\{x_n\}$ converges if and only if $\{y_n\}$ converges.
    \end{block}

    \cake{} 
    $\{y_{n}\}$ convergent 
    $\imp$
    $\{x_{n}\}$ convergent  is obvious because the former is \blankshort{} of the
    latter. (Is this ``if'' or ``only if''?)

    \hint{} $\{x_{n}\}$ convergent 
    $\imp$
    $\{y_{n}\}$ convergent needs checking the definition of convergence.
\end{frame}

\section{\acs{bai} 2.4 Cauchy sequences}

\begin{frame}
    \frametitle{Cauchy}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            As a professor of the École Polytechnique,
            Cauchy had been a notoriously bad lecturer,
            assuming levels of understanding that only a few of his best students could reach,
            and cramming his allotted time with too much material.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Cauchy.jpg}
                \caption*{Augustin-Louis Cauchy (1789--1857) 
                    was a French mathematician, engineer, and physicist}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Cauchy sequences}

    \begin{block}{Definition 2.4.1}
        A sequence $\{x_n\}$ is a Cauchy sequence if for every
        $ε > 0$ there exists an $M ∈ \dsN$
        such that for all $n ≥ M$ and all $k ≥ M$, we have
        $|x_n − x_k | < ε$.
    \end{block}

    \begin{exampleblock}{Example 2.4.2}
        The sequence $\{1 / n\}$ is a Cauchy sequence.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Cauchy sequence are bounded}

    \begin{block}{Proposition 2.4.4}
        A Cauchy sequence is bounded.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Cauchy sequence are convergent}

    \begin{block}{Theorem 2.4.5}
        A sequence of real numbers is Cauchy if and only if it converges.
    \end{block}

    \cake{} Convergence $\imp$ Cauchy is easy.

    \zany{} Cauchy $\imp$ convergence requires section 2.3 and we skipped that!
\end{frame}

\section{\acs{bai} 2.5 Series}

\subsection{2.5.1 Definition}

\begin{frame}
    \frametitle{What is a series?}

    \begin{block}{Definition 2.5.1}
        Given a sequence $\{x_n\}$, we call
        \begin{equation*}
            \sum_{n=1}^{\infty} x_{n}
        \end{equation*}
        a \alert{series}. 

        A series converges if the sequence $\{s_k\}$ defined by
        \begin{equation*}
        s_k := \sum_{n=1}^{k} x_n = x_{1} + \dots x_{k}
        \end{equation*}
        converges.

        The numbers $s_k$ are called \alert{partial sums}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 2.5.4}
    In \emoji{sweden}, it is impolite to eat the last piece of food.

    If the first Swede eats $\frac{1}{2}$ of the cake,
    the second eats $\frac{1}{4}$ of the cake, and so on.

    Will people be able to finish one \emoji{birthday}?

    \pause{}

    This is to ask what is
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{1}{2^{n}} = \blankveryshort{}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{How many \emoji{birthday} do we need?}

    \begin{block}{Proposition 2.5.5}
        Suppose $−1 < r < 1$. Then the \alert{geometric series}
        $\sum_{n=0}r^{n}$
        converges, and
        \begin{equation*}
            \sum_{n=0}r^{n} = \frac{1}{1-r}
        \end{equation*}
    \end{block}

    \astonished{} Taking $r=1/2$. This implies that we need $2$ \emoji{birthday}
    to satisfy the \emoji{sweden} people instead of $1$ as on the previous slide.
    Why?
\end{frame}

\begin{frame}
    \frametitle{The begin does not matter}

    \begin{block}{Proposition 2.5.6}
        Let $M \in \dsN$.
        Then
        \begin{equation*}
            \sum_{n=1}^{\infty} x_{n}
            \text{ converges }
            \iff
            \sum_{n=M}^{\infty} x_{n}
            \text{ converges }
        \end{equation*}
    \end{block}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \exercisepic{\lecturenum}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \assignmentlastweek{}
            \begin{itemize}
                \item[\emoji{pencil}] Exercise 2.1:9-16
                \item[\emoji{pencil}] Exercise 2.4:1-2, 4-5
                \item[\emoji{pencil}] Exercise 2.5:1-2
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
