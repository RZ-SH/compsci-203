\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 19}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 5.3 Eulerian and Hamiltonian Graphs}

\subsection{Eulerian Graphs}

\begin{frame}[c]
    \frametitle{Leonhard Euler}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Euler.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://en.wikipedia.org/wiki/Leonhard\_Euler}{Leonhard Euler} (1707--1783) was a Swiss mathematician, physicist, astronomer,
            geographer, logician and engineer who founded the studies of 
            \emph{graph theory} and topology \dots
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The seven-bridges problem}
    
    In Königsberg, there was a river and seven bridges on it.

    Was it possible to plan a walk so that you cross
    each bridge once and only once?

    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{Konigsberg_bridges.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Eulerian graphs}

    In a graph $G$ 
    a \emph{walk} $(x_{1}, \ldots, x_{t})$ is called a \alert{circuit} if 
    $x_0 = x_t$.

    If there is a circuit which traverses each edge of $G$ \alert{exactly once}, the
    graph is called an \alert{eulerian} graph.

    Such a walk is called an \alert{eulerian circuit}.

    \begin{figure}
        \centering
        \includegraphics[width=0.45\textwidth]{bridges-graph.png}
        \caption*{Is this multigraph eulerian?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A special weird case}
    
    \zany{} Is the following graph Eulerian?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{eulerian-isolated-vertex.pdf}
    \end{figure}

    \bomb{} Graphs with isolated vertices are mistakenly ignored in the textbook.
\end{frame}

\begin{frame}
    \frametitle{An eulerian graph}

    \cake{} Can you show the following graph is eulerian?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{eulerian-graph-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Euler's theory}
    
    \begin{block}{Theorem 5.13 (AC)}
        A graph $G$ \emph{which has no isolated vertex} 
        is eulerian if and only if it is connected and every vertex has even degree.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Finding an eulerian circuit}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            We gradually extends a circuit until it traverses all
            edges.

            \begin{enumerate}
                \item (1)
                \item (1, 2, 4, 3, 1)
                \item (1, 2, 5, 8, 2, 4, 3, 1)
                \item \dots
            \end{enumerate}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{circuit-1.png}
            \end{figure}
        \end{column}
    \end{columns}

    \vspace{2cm}
    
    \hint{} Always go to the lowest index!
\end{frame}

\begin{frame}
    \frametitle{A disconnected graph?}

    \think{} What if we try the same method on a disconnected graph?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{eulerian-disconnected.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{A graph with odd degrees?}

    \think{} What if we try the same method on a graph with odd degrees?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{bridges-graph.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The algorithm}
    
    To find an eulerian circuit, we run the following algorithm

    \begin{enumerate}
        \item Start with the trivial circuit $C=(1)$.
        \item\label{it:success} If $C = (x_{0}, \dots, x_{t})$ traverses all edges, then stop.
        \item Let $i$ be the least number in $[t]$ in $C$ that $x_i$ \blankveryshort{}. 
        \item If no such $i$ exists, then the graph is \blankveryshort{}.
        \item From $u_{0} = x_i$, we do a walk $(u_{0}, \ldots, u_{j})$ along only
            \blankveryshort{} edges and
            we always choosing the vertex with 
            \blankveryshort{} label as the next stop.
        \item If after $s$ steps, we reach $u_{s}$ with no \blankveryshort{}
            adjacent to it, then we stop.
        \item If $u_{s} \ne u_{0}$, then  degrees of $\u_{s}$ must be \blankveryshort{}.
        \item If $u_{s} = u_{0}$, then we replace $x_i$ by $u_{0}, \dots, u_{s}$
            and go to step-\ref{it:success}.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    \bonus{} Find an eulerian circuit in the graph $G$ using this \alert{algorithm}.
    
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{eulerian-exercise.pdf}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Hamiltonian Graphs}

\begin{frame}
    \frametitle{Hamiltonian Graphs}

    A graph $G$ is \alert{hamiltonian} if there is a cycle $C = (x_{1}, \ldots, x_{n})$ in $G$
    such that every vertices in $G$ appears \emph{exactly once} in $C$.

    The cycle $C$ is called a \alert{hamiltonian cycle}.

    \cake{} Are the graphs hamiltonian/eulerian?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{hamiltonian.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Petersen Graph}

    This is not hamiltonian.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{5.17.png}
    \end{figure}

    \emoji{eye} See a proof
    \href{https://mathworld.wolfram.com/PetersenGraph.html}{here}.
\end{frame}

\begin{frame}[t]
    \frametitle{A sufficient condition}

    \begin{block}{Theorem 5.18 (AC)}
        If $G$ is a graph on \emph{$n \ne 2$} vertices and each vertex in $G$ has at least
        $\ceil{n/2}$ neighbours, then $G$ is hamiltonian.
    \end{block} 

    \only<1>{\bomb{} The textbook missed the case $n=2$. What happens in this case?}

    \only<2>{\bomb{} Note that this theorem only gives a sufficient condition. 

    Can you think of a hamiltonian graph with $n = 5$ and maximum degree $2 <
    3 = \ceil{n/2}$?}

    \only<3>{
        \cake{} Is this graph hamiltonian?
        \vspace{1em}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.4\textwidth]{hamiltonian-example.pdf}
        \end{figure}
    }
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}
    
    \bonus{} Show that any graph $G$ with 17 vertices and 129 edges must be hamiltonian
    with Theorem 5.18.

    \hint{} 
    Note that the complete graph $K_{17}$ has $\binom{17}{2} = 136$ edges.
    Can you see why this implies that the minimum degree in $G$ is $9 \ge \ceil{17/2}$?
\end{frame}

%\begin{frame}[t]
%    \frametitle{Complete bipartite graphs}
%    
%    A complete bipartite graph $K_{m, n}$ is a bipartite graph with two parts of
%    sizes $m$ and $n$ such that every vertex in the first part is connected to every
%    vertex in the second part.
%
%    What is a necessary and sufficient condition for $K_{m,n}$ to be hamiltonian?
%\end{frame}
%
%\begin{frame}[t]
%    \frametitle{Number of components}
%
%    \begin{exampleblock}{Can you prove?}
%        If $G$ has a hamiltonian cycle, then for each nonempty set 
%        $S \subseteq V$, the graph $G \setminus S$ has at most $|S|$ vertices.
%    \end{exampleblock}
%
%    \begin{exampleblock}{Can you distinguish?}
%        Which of the following are hamiltonian.
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{hamiltonian-3.png}
%    \end{figure}
%    \end{exampleblock}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-19.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 8, 9, 10, 11, 12.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
