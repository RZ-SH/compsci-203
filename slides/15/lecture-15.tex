\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 15}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 9 Recurrence Equations}

\subsection{9.4.2 Solving Advancement Operator Equations -- Nonhomogeneous Case}

\begin{frame}
    \frametitle{Nonhomogeneous equations}
    Consider the homogeneous equation
    \[
        p(A) f = (A+2)(A-6) f = 0.
    \]
    \cake{} What is the general solution for this?
    \[
        f_{1}(n) = \blankshort{}
    \]

    \think{} What is the general solution for
    \[
        p(A) f = (A+2)(A-6) f = 3^{n}
    \]
\end{frame}

\begin{frame}{Finding a particular solution}
    \sweat{} No algorithm is known which grantees a solution of
    \[
        p(A) f = (A+2)(A-6) f = 3^{n}
    \]

    \hint{} Experience suggests trying \emph{something look like the \ac{rhs}}, say, \(d
    3^{n}\), where $d$ is to be decided.

\end{frame}

\begin{frame}
    \frametitle{When we have one solution}

    This gives one \alert{particular} solution
    \[
        f_{2}(n) = \blankshort{}
    \]

    \emoji{laughing} \emph{All} solutions of $p(A) f(n) = 3^{n}$ are of this form ---
    \[
        f(n)=f_{1}(n) + f_{2}(n).
    \]
\end{frame}

\begin{frame}
    \frametitle{A recipe \emoji{ramen} for nonhomogeneous equations}
    We want to solve problems like 
    \[
        p(A) f = g.
    \]
    First we find the \emph{general} solution \(f_{1}\) for 
    \[
        p(A) f = 0
    \]
    Second we find (any) \emph{particular} solution (by guessing) \(f_{2}\) for 
    \[
        p(A) f = g.
    \]
    Then the general solution is
    \[
        f(n)=f_{1}(n)+f_{2}(n)
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 9.15}
    
    Find the solutions to the equation
    \begin{equation*}
        (A + 2)(A − 6) f = 6^n,
    \end{equation*}
    if $f(0) = 1$ and $f(1) = 5$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.16}

    Find the solutions to the equation
    \begin{equation*}
        r_{n+1} = r_{n} + n + 1
    \end{equation*}
    and $r(0) = 1$ and $r(1) = 2$.
\end{frame}

\begin{frame}
    \frametitle{Example --- Towers of Hanoi puzzle}

    These disks are to be transferred, one at a time, onto another peg, 
    such that at no time is a larger disk is on top of a smaller one. 

    Let $h_n$ be the minimal number of moves needed.

    Find a recursions for $h_{n}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{Tower_of_Hanoi.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Solve the recursion in the Hanoi puzzle.
\end{frame}

\subsection{9.5 Formalizing our approach to recurrence equations}

\begin{frame}[c]
    \frametitle{Formalizing everything}
    
    \begin{block}{Theorem 9.18. (AC)}
        Let $k$ be a positive integer $k$, 
        and let $c_0 , c_1 , \ldots, c_k$ be constants with 
        $c_0 , c_k \ne 0$.

        Then the set $W$ of all solutions to the homogeneous linear equation
        \begin{equation*}
            (c_0 A_k + c_1 A_{k−1} + c_2 A_{k-2} + \ldots + c_k) f = 0 
            \tag{9.5.1}
        \end{equation*}
     is a $k$-dimensional subspace of $V$.
    \end{block}

    \emoji{laughing} \emph{All} solutions are linear combinations of $k$
    linearly-independent solutions (the basis of $W$).

    \emoji{eye} See pp.\ 474--480 of this \href{https://hefferon.net/linearalgebra/}{linear
    algebra textbook} for a proof.
\end{frame}

\begin{frame}[t]{Example 9.9 again}
    Consider
    \begin{equation}
        \label{eq:homogeneous:1}
        p(A) f 
        = (A^{2}+A-6) f 
        = (A+3)(A-2)f 
        = 0
    \end{equation}

    Its solution space has dimension $\underline{\hspace{1cm}}$.

    Note that $2^{n}$ and $(-3)^{n}$ are two linearly independent solutions.

    Thus
    \begin{itemize}
        \item Then \(c_{1} 2^{n} + c_{2} (-3)^{n}\) are also solutions of \cref{eq:homogeneous:1}.
        \item \alert{All} solutions are of this form.
    \end{itemize}
\end{frame}

\subsection{9.6 Using Generating Functions to Solve Recurrences}

\begin{frame}
    \frametitle{An old problem}
    
    Recall that we have studied how to solve the recursion
    \begin{equation*}
        (A^{2} + A − 6) r = 0,
    \end{equation*}
    with initial conditions $r_{0} = 1$, $r_{1} = 3$.

    This means
    \begin{equation*}
        r_{n+2} + r_{n+1} - 6 r_{n} = 0.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A new trick}
    
    The GF of $(r_{n})_{n \ge 0}$ is
    \begin{equation*}
        f(x) = r_{0} + r_{1} x + r_{2} x^{2} + \cdots
    \end{equation*}

    \think{} How can we show that
    \begin{equation*}
        f (x) 
        = \frac{1 + 4x}{1 + x − 6x^2}
        .
    \end{equation*}
\end{frame}


\begin{frame}
    \frametitle{Partial fractions}
    \emoji{robot} To get coefficients of $f(x)$, we can use
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5B+\%281+\%2B+4x\%29\%2F\%281+\%2B+x+\%E2\%88\%92+6x\%5E2+\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}.

    \emoji{muscle} Or we can write $f(x)$ as 
    \href{https://en.wikipedia.org/wiki/Partial\_fraction\_decomposition}{partial
    fractions} ---
    \begin{equation*}
        f (x) 
        = 
        \frac{1 + 4x}{1 + x − 6x^2}
        =
        \frac{1+4 x}{(1+3 x) (1-2 x)}
        =
        \frac{6}{5 \left(1-2 x \right)}
        -
        \frac{1}{5 \left(1+3 x \right)} 
        .
    \end{equation*}

    \cake{} How to get $r_n$ from this?
\end{frame}

\begin{frame}
    \frametitle{Non-homogeneous recurrence}

    Assume that $r_{0}=2$, $r_{1}=1$ and for $n \ge 2$
    \begin{equation*}
        r_{n} − r_{n−1} − 2 r_{n−2} = 2^n.
    \end{equation*}
    Then the generating function of $(r_{n})_{n \ge 0}$ is
    \begin{equation*}
        R(x) = \frac{6x^2 − 5x + 2}{(1 − 2x)(1 − x − 2x^2)}
        .
    \end{equation*}

    To find coefficients with \emoji{pencil}, use partial fractions.
\end{frame}


\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-14.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 9.9: 9, 11, 13, 15, 17.
            \end{itemize}

            \hint{} For some problems, you can use WolframAlpha like 
            \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{this}.
        \end{column}
    \end{columns}
\end{frame}

\end{document}
