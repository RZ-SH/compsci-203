\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 18}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 5.6 Counting Labelled Trees --- Cayley's formula}

\subsection{A Proof Using Prüfer code}

\begin{frame}
    \frametitle{Trees of size $2, 3$ and $4$}
    The number of (labelled) trees of size $1, 2, 3, \ldots 10 $ are
    \begin{equation*}
        1, 1, 3, 16, 125, 1296, 16807, 262144, 4782969, 100000000
    \end{equation*}
    \think{} Can you \href{https://oeis.org/A000272}{guess} the number of trees of size $n$?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{tree.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cayley's formula}
    
    \begin{block}{Theorem 5.39 (AC)}
        The number $T_n$ of labelled trees on n vertices is $\underline{\hspace{1cm}}$.
    \end{block}
    
    \cake{} Can you find the number of rooted trees of size $n$ using Cayley's formula?
\end{frame}

\begin{frame}
    \frametitle{Prüfer code}
    
    One proof of Cayley's formula uses Prüfer code which 
    maps a tree of size $n$ to a string of length $n-2$ with alphabet $[n]$.

    It works like this ---

    \begin{itemize}
        \item If $T = K_2$, return the empty string.
        \item  Else, let $v$ be the leaf with the smallest label 
            and let $u$ be its unique neighbour.  
            Let $i$ be the label of $u$. Return $(i, \mathrm{prüfer}(T − v))$.
    \end{itemize}

    \cake{} How many such strings exist?

    \cake{} Why is $u$ unique?
\end{frame}

\begin{frame}
    \frametitle{Prüfer code}

    What is the Prüfer code of this tree?

    \begin{columns}
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=\linewidth]{tree.pdf}
        \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{A key observation}
    
    \hint{} Let $S$ be the set of letters in $\mathrm{prüfer}(T)$. 
    Then the leaves of $T$ are precisely $I = [n] \setminus S$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{tree.pdf}
        \caption{$\mathrm{prüfer}(T) = 6 6 4 3 1 4 3$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{From code to tree}
    
    \think{} Can you turn Prüfer code $75531$ to a tree?

    \begin{figure}[htpb]
        \centering
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Proof by induction}
    
    We want to show the following is true for all $m \ge 2$ ---
    \begin{itemize}
        \item $P(m)$ --- For every string $s$ of length $m-2$ with alphabet $[m]$,
            there exists a unique tree $T$ such that $\mathrm{prüfer}(T) = s$.
    \end{itemize}

    Induction basis: $P(2)$ is true.

    Induction step: Assume $P(m)$ is true for some $m \ge 2$, show that $P(m+1)$ is
    true.
\end{frame}

\begin{frame}
    \frametitle{How to generate all trees?}
    \think{} Can you write some code to enumerate all trees of size $5$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{tree-grid-5.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{} --- Trees with fixed degrees}
    Let $ (d_{1}, \ldots, d_{5}) = (3, 2, 1, 1, 1)$.

    Consider a tree $T$ of size $5$ in which node $i$ has degree $d_{i}$.

    \bonus{} Answer the following questions
    \begin{itemize}
        \item How many times does the letter $i$ appear in $\mathrm{prüfer}(T)$.
        \item What are all the possible Prüfer codes of $T$?
        \item How many such trees are there?
    \end{itemize}
\end{frame}

%\begin{frame}
%    \frametitle{Trees with fixed degrees}
%    Let $d_{1}, \cdots d_{n}$ be positive integers summing to $2(n-1)$.
%
%    Consider a tree $T$ of size $n$ in which node $i$ has degree $d_{i}$.
%
%    \think{} Answer the following questions
%    \begin{itemize}
%        \item How many times does $i$ appear in $\mathrm{prüfer}(T)$.
%        \item What are all the possible Prüfer codes of $T$?
%        \item How many such trees are there?
%    \end{itemize}
%\end{frame}

\subsection{A Proof Using Generating Functions \zany{}}

\begin{frame}
    \frametitle{\ac{egf} for labelled trees}

    Let $c_{n}$ be the number of rooted trees with $n$ vertices.
    The \ac{egf} of $(c_n)_{n \ge 0}$ satisfies
    \begin{equation*}
        T(x) 
        =
        x 
        \left(1 + T(x) 
        + 
        \frac{T(x) T(x)}{2!}
        +
        \frac{T(x) T(x) T(x)}{3!}
        \cdots
        \right)
        = x e^{T(x)}
    \end{equation*}
    In other words
    \begin{equation*}
        x = T(x) e^{-T(x)}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Lagrange–Bürmann formula}

    A \href{https://wolfr.am/17AlXkg7q}{formula} often used in combinatorics is the
    following --

    Assume that $f(w) = w/\phi(w)$.
    Then the inverse of $f$, $g(x)$ (satisfying $g(f(x)) = x$) has coefficients
    \begin{equation*}
        [x^{n}] g(x) = \frac{1}{n} [w^{n-1}] \phi(w)^{n}.
    \end{equation*}

    \cake{} How to apply this to get $c_{n}$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Let $t_{n}$ be the number of rooted labelled trees in which each node has $0$ or
    $1$ child.

    Let $T(x)$ be the \ac{egf} of $(t_{n})_{n \ge 0}$.

    Can you get recursive formula of $T(x)$ in the form of
    \begin{equation*}
        T(x) = x + x \cdot \underline{\hspace{1cm}}
    \end{equation*}

    Can you use this formula to get a closed formula of $t_{n}$?
\end{frame}

\begin{frame}[c]
    \frametitle{Phillip Flajolet}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{PhilippeFlajolet.jpg}
            \end{figure}        
        \end{column}
        \begin{column}{0.5\textwidth}
            Philippe Flajolet (1948 - 2011) was a French computer scientist who,
            among many other things, 
            \href{https://arxiv.org/pdf/math/0411250.pdf}{studied trees with
            generating functions}.
        \end{column}
    \end{columns}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-16.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 37, 39, 41.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
