\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\usepackage{csquotes}

\begin{document}

\maketitle

\section{\acs{bai} 2.5 Series}

\subsection{2.5.5 Comparison test and the $p$-series}

\begin{frame}
    \frametitle{Comparison test}
    
    \begin{block}{Proposition 2.5.16}
        Let $∑ x_n$ and $∑ y_n$ be series such that $0 ≤ x_n ≤ y_n$ for all
        $n ∈ \dsN$.

        (i) If $∑ y_n$ converges, then so does $∑ x_n$.

        (ii) If $∑ x_n$ diverges, then so does $∑ y_n$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{$p$-test}
    
    \begin{block}{Proposition 2.5.17}
        For $p ∈ \dsR$, the series
        \begin{equation*}
            \sum_{n=1}^{\infty} \frac{1}{n^{p}}
        \end{equation*}
        converges if and only if $p > 1$.
    \end{block}

    \pause{}

    \begin{block}{Example 2.5.18}
        The series $∑ \frac{1}{n^2+1}$ converges.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Converge only conditionally (again)}
    
    Show that
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{(-1)^{n}}{n}
    \end{equation*}
    converges.
\end{frame}

\subsection{2.5.6 Ratio test}

\begin{frame}
    \frametitle{Ratio test}
    
    \begin{block}{Proposition 2.5.19}
        Let $∑ x_n$ be a series, $x_n \ne 0$ for all $n$, and such that
        \begin{equation*}
            L
            \coloneq
            \lim_{n \to \infty} \frac{\abs{x_{n+1}}}{\abs{x_n}}
            \qquad
            \text{exists.}
        \end{equation*}

        (i) If $L < 1$, then $∑ x_n$ converges absolutely.

        (ii) If $L > 1$, then $∑ x_n$ diverges.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Try ratio test}
    
    \begin{exampleblock}{Example 2.5.20}
    The series
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{2^n}{n!}
    \end{equation*}
    converges absolutely.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Which of the following converge? Why?

    \begin{enumerate}
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{3}{9n+1}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{1}{2n-1}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{(-1)^n}{n^2}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{1}{n(n+1)}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{n}{e^{n^{2}}}$
    \end{enumerate}
\end{frame}

\section{2.6 More on series}

\subsection{2.6.2 Alternating series test}

\begin{frame}
    \frametitle{Alternating series}

    \begin{block}{Proposition 2.6.2}
        Let $\{x_n\}$ be a monotone decreasing sequence
        of positive real numbers such that $\lim_{n \to \infty} x_n = 0$. 
        Then
        \begin{equation*}
            \sum_{n=1}^{\infty} (-1)^n x_n
        \end{equation*}
        converges.
    \end{block}
\end{frame}

\subsection{2.6.5 Power series}

\begin{frame}[c]
    \frametitle{What is $2^{π}$?}
    
    If you ask a \emoji{robot}, you will get
    \begin{equation*}
        2^{π} \approx 8.824977827076287
    \end{equation*}
    \think{} But what does $2^{π}$ defined?
\end{frame}

\begin{frame}
    \frametitle{\emoji{muscle} series}
    
    Fix $x_0 ∈ \dsR$. 
    A \alert{power series about $x_0$} is a series of the form
    \begin{equation*}
        \sum_{n=0}^{\infty} a_{n}(x-x_{0})^{n}.
        \label{eq:power}
    \end{equation*}

    For $x$ which makes the series convergent, this defines a function of $x$.

    \cake{} Can you think of an $x$ which always makes the series convergent
    regardless of $a_{n}$?
\end{frame}

\begin{frame}[c]
    \frametitle{Some functions defined by power series}
    
    Many common functions are defined by power series.
    For example
    \begin{equation*}
        e^{x} = \sum_{n = 0}^{\infty} \frac{1}{n!} (x-0)^n
    \end{equation*}
    and
    \begin{equation*}
        \log(x) = \sum_{n=0}^{\infty} \frac{(-1)^{n}}{n} (x-1)^{n}
    \end{equation*}
    and
    \begin{equation*}
        x^{\pi} = e^{\pi \log(x)}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{When does it converge?}

    \begin{block}{Example 2.6.8}
        What $x$ makes the series
        \begin{equation*}
            \sum_{n=1}^{\infty} \frac{1}{n} x^{n}
        \end{equation*}
        converge absolutely, converge conditionally, diverge?
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Radius of convergence}
    
    \begin{block}{Proposition 2.6.10}
    If a series converges for some $x \ne x_{0}$, 
    then either it converges at all $x ∈ \dsR$, 
    or there exists $ρ$, 
    such that the series converges absolutely on
    the interval $(x_0 − ρ, x_0 + ρ)$ 
    and diverges when $\abs{x} > x_0 + ρ$.
    \end{block}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=1.0\textwidth]{figure-2-8.png}
    \end{figure}

    The number $ρ$ is called the \alert{radius of convergence} of the power series.
\end{frame}

\section{Real Analysis in Algorithm Analysis}
    
\begin{frame}
    \frametitle{Quick-sort}
    
    The quick-sort algorithm sorts a list of numbers 
    by 
    \begin{enumerate}
        \item picking a random pivot number $p$
        \item split the list into two lists --- these less than $p$ and these greater than $p$,
        \item sort these two lists with quick-sort.
    \end{enumerate}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{quick-sort.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Analysis of Quick-sort}
    
    Given $n$ numbers, the average running time of quick-sort is
    \begin{equation*}
        c_n = 2(n + 1)H_n − 4n
    \end{equation*}
    where
    \begin{equation*}
        H_{k} \coloneq \sum_{n=1}^{k} \frac{1}{n}
        .
    \end{equation*}

    From real analysis, we know that $H_{n}$ diverges, thus $c_{n}$ diverges.

    \think{} But can we say how fast $c_n$ increases?
\end{frame}

\begin{frame}
    \frametitle{Big $O$ notation}
    
    In analysis, we write
    \begin{equation*}
        f(n) = O(g(n))
    \end{equation*}
    to denote that
    there exists a constant $C$
    \begin{equation*}
        \abs{f(n)}
        \le
        C \abs{g(n)}
        \qquad
        \text{for all}
        \,
        n \in \dsN.
    \end{equation*}
    This notation captures how \emoji{zap} the function $f(n)$ increases.
\end{frame}

\begin{frame}
    \frametitle{Example of big $O$}
    
    The sum of first $n$ squares is
    \begin{equation*}
        \square{}_n
        \coloneq
        \sum_{k=1}^{n} k^2
        =
        \frac{1}{3} n (n + \frac{1}{2}) (n+1)
        =
        \frac{1}{3} n^{3} + \frac{1}{2} n^{2} + \frac{1}{6} n
    \end{equation*}
    Then
    \begin{equation*}
        \square{}_n
        =
        O(n^{3})
    \end{equation*}
    \pause{}
    and
    \begin{equation*}
        \square{}_n
        =
        \frac{1}{3} n^{3} + O\left(n^{2}\right)
    \end{equation*}
    \pause{}
    \begin{equation*}
        \square{}_n
        =
        \frac{1}{3} n^{3} + \frac{1}{2} n^{2} + O(n).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Back to Quick-sort}
    From analysis, we have
    \begin{equation*}
        H_{n} = \log(n) + \gamma + O\left(\frac{1}{n}\right)
    \end{equation*}
    where $\gamma$ is \href{https://en.wikipedia.org/wiki/Euler\%27s\_constant}{Euler's constant}.

    Thus
    \begin{equation*}
        c_n 
        = 
        2(n + 1)H_n − 4n
        =
        2 n \log (n)
        +
        O\left(n\right)
        .
    \end{equation*}
    \cool{} So quick-sort is pretty fast!
\end{frame}

\begin{frame}
    \frametitle{The end!}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{run.jpg}
    \end{figure}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \exercisepic{\lecturenum}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \assignmentlastweek{}
            \begin{itemize}
                \item[\emoji{pencil}] Exercise 2.5:1-4, 6, 8, 10-12, 14.
                \item[\emoji{pencil}] Exercise 2.6:1, 5, 6.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
