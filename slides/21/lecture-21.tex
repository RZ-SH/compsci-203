\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\usepackage{tikz}

\title{Lecture 21}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 5.4.2 Cliques and Chromatic Number}

\begin{frame}
    \frametitle{Warming up \emoji{hot-face}}

    \cake{} What is the chromatic number of the following graph?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{clique.png}
    \end{figure}

    \hint{} If $H$ is a subgraph of $G$, then $\chi(G) \ge \chi(H)$.
\end{frame}

\begin{frame}
    \frametitle{Cliques}
    
    A subset of vertices $S$ forms a \alert{clique} if the subgraph induced by $S$ is
    isomorphic to $K_{\abs{S}}$.

    The \alert{clique number} of $G$, $\omega(G)$, is the size of the largest clique in $G$.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{clique.png}
    \end{figure}

    \cake{} Why is $\chi(G) \ge \omega(G)$.
\end{frame}

\begin{frame}[c]
    \frametitle{\cake{} What is the clique numbers}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{clique-number-01-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Clique number and chromatic number}
    \begin{block}{Proposition 5.25 (AC)}
        For every $t \ge 3$, there exists a graph $G_t$ so that $\chi(G_t) = t$ and
        $\omega(G_t) = 2$.
    \end{block}

    \hint{}
    In other words, there exist triangle-free graphs with large chromatic number.

    \bomb{} So $\omega(G)$ is not necessarily a good lower bound for $\chi(G)$.
\end{frame}

\subsection{Proof by J.~Mycielski}

\begin{frame}
    \frametitle{Proof by Induction}
    
    Base case $t=3$, let $G_{3} = C_{5}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{gg-4-0.pdf}
    \end{figure}

    \cake{} What are $\omega(G_{3})$ and $\chi(G_{3})$?
\end{frame}

\begin{frame}
    \frametitle{Construct $G_{4}$}
    
    We are going to construct $G_{4}$ such that
    $\omega(G_{4}) = 2$ and $\chi(G_{4}) = 4$.

\end{frame}

\begin{frame}
    \frametitle{Three different ways to draw $G_{4}$}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\linewidth]{gg-4-1.pdf}%
        \includegraphics<2>[width=0.7\linewidth]{gg-4-2.pdf}%
        \includegraphics<3>[width=0.7\linewidth]{gg-4-3.pdf}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{How to colour $G_{4}$ with $4$ colours?}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{gg-4-4.pdf}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Construct $G_{t+1}$}
    
    Given $G_{t}$ with
    $\omega(G_{t}) = 2$ and $\chi(G_{t}) = t$,
    we construct $G_{t+1}$ as follows ---

    \begin{itemize}
        \item Let $n_{t}$ be the number of vertices in $G_{t}$.
        \item Add new vertices $y_{1}, \ldots, y_{n_{t}}$ to $G_{t}$.
        \item Connect $y_{j}$ to $x_{i}$ if $x_{i}$ is adjacent to $x_{j}$.
        \item Add another vertex $z$ and connect it to all $y_{i}$.
    \end{itemize}

    We will show that
    $\omega(G_{t+1}) = 2$ and $\chi(G_{t+1}) = t+1$.
\end{frame}

\begin{frame}[t]
    \frametitle{Why is that $\omega(G_{t+1}) = 2$?}

    \begin{columns}
        \begin{column}{0.5\textwidth}

            \think{} 
            If there is a $K_{3}$ (\emoji{small-red-triangle}) in $G_{t+1}$,
            can it ---

            \begin{itemize}
                \item contain $z$?
                \item contain $x_i, x_j, x_k$?
                \item contain $y_i, y_j, y_k$?
                \item contain $x_i, y_j, y_k$?
                \item contain $\blankshort{}$?
            \end{itemize}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{gg-4-3.pdf}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Why is that $\chi(G_{4}) = 4$?}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{gg-4-5.pdf}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Why is that $\chi(G_{5}) = 5$?}

    \sweat{} Things get complicated soon!

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{gg-5.pdf}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Why is that $\chi(G_{t+1}) = 2$?}

    \cake{}  Why is $t \le \chi(G_{t+1})$?  

    \think{} Can it be $\chi(G_{t+1}) = t$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Note that in the proof, we changed the colour of $x_{i}$ to $y_{i}$,
    which is not $t$,
    and claimed that we get a proper colouring with $t$ colours?

    But what if $x_{i}$ is adjacent to $y_{j}$ which has colour $t$?
    Do we not get a conflict in colouring? \zany{}
\end{frame}

\subsection{Proof by J.~Kelly and L.~Kelly}

\begin{frame}
    \frametitle{Cakes!}

    There are $2 \times 5 + 1 = 11$ students in the class.

    The professor bought $5$ \emoji{cupcake} for them.

    Is it possible that all students get a piece of \emoji{cupcake},
    but no \emoji{cupcake} is shared by more than $2$ students?
\end{frame}

\begin{frame}
    \frametitle{More cakes!}

    There are $(m-1) \times n + 1$ students in the class.

    The professor bought $n$ \emoji{cupcake} for them.

    Is it possible that all students get a piece of \emoji{cupcake},
    but no \emoji{cupcake} is shared by more than $m-1$ students?
\end{frame}
\begin{frame}
    \frametitle{Generalized \emoji{bird} Hole Principle}

    \begin{block}{Proposition 5.24 (AC)}
        If $f : X \mapsto Y$ is a function and
        $|X| ≥ (m − 1)|Y| + 1$, 
        then there exists an element $y ∈ Y$ and distinct 
        elements $x_1 , \ldots, x_m ∈ X$ so that 
        $f (x_i ) = y$ for $i = 1, \ldots, m$.
    \end{block}

    \hint{} There are going to be $m$ student students who share a \emoji{cupcake}.
\end{frame}

\begin{frame}
    \frametitle{We use the same $G_{3}$ as before}

    Base case $t=3$, let $G_{3} = C_{5}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{gg-4-0.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{But $G_{4}$ is complicated! \zany{}}

    \emoji{worried} It is not really possible to draw $G_4$!
    This is just a sketch.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{gg-4-kelly.pdf}
    \end{figure}

    For each $5$-subset of the $13$ \emoji{o}, attach a copy of $G_{3}$.

    Note that $G_4$ has no \emoji{small-red-triangle}.
\end{frame}

\begin{frame}
    \frametitle{Why $\chi(G_{4}) = 4$?}

    Assume that $\chi(G_{4}) = 3$.

    \emoji{bird}-hole-principle $\implies$ Then among $13$ \emoji{o}, at least $5$ will have the same colour.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{gg-4-kelly.pdf}
    \end{figure}

    \think{} Why is this a contradiction?
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-21.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 19, 21, 22, 23.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
