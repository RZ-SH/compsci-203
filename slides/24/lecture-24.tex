\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 11 Applying Probability to Combinatorics}

\subsection{11.1 A First Taste of Ramsey Theory}

\begin{frame}
    \frametitle{Six students come to the class}
    
    Six students are taking the course COMPSCI 203.

    The \teacher{} says ``I bet 100 \emoji{dollar} that either three of you who knew
    each other from before this session, or three of you did not know each other from
    before.''

    \think{} Should you take the bet?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            [
            scale=1.6,
            inner sep=1.5mm,
            vertex/.style={opacity=0,text opacity=1,font=\Large},
            every path/.style={lightgray,ultra thick},
            ]
            \def\utilities{water,electricity,natural gas}
            \foreach \s in {1, ..., 6}{%
                \pgfmathsetmacro{\x}{cos(\s*pi/3 r)}
                \pgfmathsetmacro{\y}{sin(\s*pi/3 r)}
                \node (\s) at (\x, \y) [vertex]
                    {\includegraphics[width=.1\textwidth]{boy.png}};
            }
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Some more examples}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{ramsey-party.pdf}
        \caption{Some possible cases}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{A six student lemma}
    
    \begin{block}{Lemma 11.1 (AC)}
        Let $G$ be any graph with six vertices. 
        Then either $G$ contains a $K_{3}$ or an $I_{3}$ as subgraphs.
    \end{block}

    \pause{}

    \hint{} Note that $C_{5}$ contains neither $K_{3}$ or $I_{3}$. 

    \cake{} What about a graph with $7, 8, 9, \ldots$ vertices?
\end{frame}


\begin{frame}
    \frametitle{Ramsey's theory for graphs}

    \begin{block}{Theorem 11.2 (AC)}
        If $m, n \in \{1,2,3\dots\}$,
        then there exists a least positive integer $R(m, n)$ 
        so that if $G$ is a graph with at least $R(m, n)$ vertices, 
        then either $G$ contains $K_{m}$ or $I_{n}$.
    \end{block}

    \cake{} What is $R(3,3)$?

    \pause{}

    \cake{} What is $R(1, 1)$? 

    \pause{}

    \think{} What about $R(2, 1), R(3, 1), R(4,1), \ldots$?

\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    What is $R(2, 2), R(3, 2), R(4, 2),$? 

    Can you guess what is $R(m, 2)$ for any $m$?
\end{frame}

\begin{frame}[t]
    \frametitle{Proof of Theorem 11.2}

    We use induction on $m+n$ to show that 
    \begin{equation*}
        R(m,n) \le \binom{m+n-2}{m-1}.
    \end{equation*}

    Induction basis: Trivial to verify for $m + n \le 5$.

    \pause{}

    Induction step: 
    Assume true for $m + n = t - 1$.

    Let $x$ be any vertex in $G$ with $\binom{m+n-2}{m-1}$ vertices where $m + n
    = t$.

    We show there there is either a $K_{m}$ or an $I_{n}$ using the six-student
    argument.
\end{frame}

\subsection{11.2 Small Ramsey Numbers}

\begin{frame}[t]
    \frametitle{$R(4,3)$}

    Note that $R(4,3) \le \binom{4+3-2}{3-1} = 10$.

    The following graphs prove that $R(4,3) > 8$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{ramsey-cyc.pdf}
    \end{figure}

    In fact $R(4, 3) = 9$. (Assignment!)
\end{frame}

\begin{frame}[c]
    \frametitle{Small Ramsey Numbers}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{ramsey.png}
        \caption{Small Ramsey numbers}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Erdős and Ramsey numbers}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Say \emoji{alien} want to know $R(5,5)$,
            or they'll wipe out the whole human race.

            Then we should join all our forces to find $R(5,5)$.

            But if they ask for the value of $R(6,6)$,
            we should instead try to destroy the \emoji{alien}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Erdos.jpg}
                \caption{From
                    \href{https://en.wikipedia.org/wiki/Paul_Erd\%C5\%91s}{Wikipedia}}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{11.3 Estimate Ramsey Number}

\begin{frame}[c]
    \frametitle{Stirling's approximation}
    
    To approximate $n!$, we can use \href{https://mathworld.wolfram.com/StirlingsApproximation.html}{Stirling's approximate}
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+\frac{1}{12
        n}+\frac{1}{288 n^2}-\frac{139}{51840
    n^3}+O\left(\left(\frac{1}{n}\right)^4\right)\right),
    \end{equation*}
    or the simpler version
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+O\left(\frac{1}{n}\right)\right).
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{A numeric verification}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{ramsey-stirling.pdf}
        \caption{$n!/\sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{How to Estimate Ramsey Number?}

    Given that
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+O\left(\frac{1}{n}\right)\right).
    \end{equation*}
    we have
    \begin{equation*}
        R(n,n) \le \binom{2 n - 2}{n-1} \approx \frac{2^{2n}}{4 \sqrt{\pi n}}.
    \end{equation*}
\end{frame}

\subsection{11.5 Ramsey’s Theorem}

\begin{frame}[c]
    \frametitle{Another bet}
    
    Sixteen students are taking the course COMPSCI 203.

    The professor say ``I bet 100 \emoji{dollar} that either three of you are
    friends, or three of you are enemies, or three of you who are neutral to each
    other''

    Will the professor always win this bet?
\end{frame}

\begin{frame}
    \frametitle{Graph theory version}
    This is the same of asking, if a graph has $16$ vertices and we colour its edges
    with three colours, do we always find a monochromatic $K_{3}$?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{K_16_partitioned_into_three_Clebsch_graphs.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A bit notations}
    
    Let $C(S, k)$ be the set of all $k$-subset of ${S}$.
    
    \cake{} What is 
    \begin{itemize}
        \item $C(\{\temoji{apple}, \temoji{banana}, \temoji{carrot}\}, 1)$, 
        \item $C(\{\temoji{apple}, \temoji{banana}, \temoji{carrot}\}, 2)$, 
        \item $C(\{\temoji{apple}, \temoji{banana}, \temoji{carrot}\}, 3)$?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem -- two colours}

    \begin{block}{Theorem 11.6 (AC)}
        Let $\mathbf{h} = (h_1 , h_2)$ be a list of integers 
        with $h_i ≥ 1$ for each $i \in [2]$. 

        Then there exists a least positive integer $R(\mathbf{h})$ so that if $φ : C([n], 2) \mapsto [2]$ is any function, 
        then there exists an integer $α ∈ [2]$ and a subset $H_α ⊆ [n]$ with $|H_α| =
        h_α$ so that $φ(S) = α$ for every $S ∈ C(H_α, 2)$.
    \end{block}

    This is exactly the same as Theorem 11.2
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem -- three colours}

    \begin{block}{Theorem 11.6 (AC)}
        Let $\mathbf{h} = (h_1 , h_2, h_{3})$ be a list of integers 
        with $h_i ≥ 1$ for each $i \in [3]$. 

        Then there exists a least positive integer $R(\mathbf{h})$ so that if $φ : C([n], 2) \mapsto [3]$ is any function, 
        then there exists an integer $α ∈ [3]$ and a subset $H_α ⊆ [n]$ with $|H_α| =
        h_α$ so that $φ(S) = α$ for every $S ∈ C(H_α, 2)$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem -- $r$ colours}

    \begin{block}{Theorem 11.6 (AC)}
        Let $r$ be a positive integer.

        Let $\mathbf{h} = (h_1 , h_2, \dots, h_{r})$ be a list of integers 
        with $h_i ≥ 1$ for each $i \in [r]$. 

        Then there exists a least positive integer $R(\mathbf{h})$ so that if $φ : C([n], 2) \mapsto [r]$ is any function, 
        then there exists an integer $α ∈ [r]$ and a subset $H_α ⊆ [n]$ with $|H_α| =
        h_α$ so that $φ(S) = α$ for every $S ∈ C(H_α, 2)$.
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{Ramsey's theorem -- the most generous version}
%
%    \begin{block}{Theorem 11.6 (AC)}
%        Let $r$ and $s$ be positive integers and 
%        let $\mathbf{h} = (h_1 , h_2 , \dots, h_r)$ be a list of integers 
%        with $h_i ≥ s$ for each $i \in [r]$. 
%
%        Then there exists a least positive integer $R(s,\mathbf{h})$ so that if $φ : C([n], s) \mapsto [r]$ is any function, 
%        then there exists an integer $α ∈ [r]$ and a subset $H_α ⊆ [n]$ with $|H_α| =
%        h_α$ so that $φ(S) = α$ for every $S ∈ C(H_α, s)$.
%    \end{block}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            Prove that
            \begin{itemize}
                \item \href{https://en.wikibooks.org/wiki/Combinatorics/Schur\%27s\_Theorem}{Schur's Theorem}
                \item
                    \href{https://www.cut-the-knot.org/arithmetic/combinatorics/Ramsey43.shtml}{$R(4,3) = 9$}
                \item
                    \href{https://www.cut-the-knot.org/arithmetic/combinatorics/Ramsey44.shtml}{$R(4,4) = 18$}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
