# A script to build all tex file

function compile(dir, file, logfile)
    cd(dir)
    try
        run(`lualatex $file`)
    catch
        open(logfile, "w+") do io
            write(io, "Failed to compile $file")
        end;
    end
end

function main()
    log = tempname() .* ".log"
    rootdir = abspath(".")
    for (root, _, files) in walkdir(rootdir)
        for f in files
            _, ext = splitext(f)
            if ext == ".tex" && root != rootdir
                dir = abspath(root)
                compile(dir, f, log)
            end
        end
    end
    cd(rootdir)
    println("See log in $log")
end

main()
