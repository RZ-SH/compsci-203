\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 10}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{AC 7 Inclusion-Exclusion}

\subsection{7.4 Derangement}

\begin{frame}
    \frametitle{Swedish graduations}

    It is a tradition for students in Sweden to wear a white cap on their graduation day.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{cap.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A \emoji{billed-cap} puzzle}
    
    Suppose that $20$ Swedish students 
    get very excited in their graduation ceremony and throw their \emoji{billed-cap} into the air. 

    Each student gets a \emoji{billed-cap} back uniformly at random.

    What is the probability that \emph{no one} gets his/her own \emoji{billed-cap} back?
\end{frame}

\begin{frame}[t]
    \frametitle{Derangement}
    
    Fix a positive integer $n$, let $X$ denote the set of all permutations on
    $[n]$. 

    A permutation $\sigma \in X$ is called a \alert{derangement} if $\sigma(i) \ne i$ for all
    $i \in [n]$.

    \cake{} Which of the two following permutations are derangements?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{derangement.png}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Count $N(S)$}
    
    Let $P_{i}$ denote the property of $\sigma$ that $\sigma(i) = i$.

    \cake{} If $n=5$, what is $N(\{1\})$, $N(\{1,2\})$, $N(\{1,2,3\})$?
\end{frame}

\begin{frame}[t]
    \frametitle{Count $N(S)$}
    
    \begin{exampleblock}{Lemma 7.10. (AC)}
    For each subset $S \subseteq [n]$ with $|S|=k$, we have
        \begin{equation*}
            N(S) = (n − k)!
        \end{equation*}
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Count derangements}
    \begin{exampleblock}{Theorem 7.11 (AC)}
        For each positive integer $n$,
        the number $d_n$ of derangements of $[n]$ satisfies
        \begin{equation*}
            d_n=\sum_{k=0}^n (-1)^k\binom{n}{k}(n-k)!.
        \end{equation*} 
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{The \emoji{billed-cap} puzzle}
    
    Suppose that $20$ Swedish students 
    get very excited in their graduation ceremony and throw their \emoji{billed-cap} into the air. 

    Each student gets a \emoji{billed-cap} back uniformly at random.

    What is the probability that \emph{no one} gets his/her own \emoji{billed-cap} back?

    \pause{}

    The answer is
    \begin{equation*}
        \frac{d_{20}}{20!} = \frac{4282366656425369}{11640679464960000} \approx 0.36787944117144232161
    \end{equation*}

    What is this \href{https://www.wolframalpha.com/input/?i=0.36787944117144232161}{number}?
\end{frame}


\begin{frame}
    \frametitle{When $n$ grows large}
    The plot of 
    \begin{equation*}
        \log\left(\frac{d_{n}}{n!}- e^{-1}\right).
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{derangement-1.png}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{The limit}

    \begin{exampleblock}{Theorem 7.12}
    For a positive integer $n$, let $d_n$ denote the number of derangements of $[n]$.
    Then 
    \begin{equation*}
        \lim_{n \to \infty} \frac{d_{n}}{n!} = \frac{1}{e}.
    \end{equation*}
    \end{exampleblock}

    \hint{} Recall from calculus that
    \begin{equation*}
        e^x = \lim_{n \to \infty} \sum_{i=1}^{n} \frac{x^{i}}{i!}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 7.1.3}
    
    Suppose instead of requiring all $20$ students to \emph{not} to have their \emoji{billed-cap}
    back, we want insist that precisely $5$ of them \emph{get} their own \emoji{billed-cap}.

    \cake{} How many ways can we do this?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Suppose that among the $20$, student $2$ gets \emoji{billed-cap} $1$.

    Consider two cases:

    \begin{enumerate}
        \item Student $1$ gets \emoji{billed-cap} $2$.
        \item Student $1$ does not gets \emoji{billed-cap} $2$.
    \end{enumerate}

    In how many ways can we finish the derangement in each case?

    \hint{} Your answer should use $d_{19}$ and $d_{18}$.
\end{frame}

\begin{frame}
    \frametitle{A recursive formula}
    
    We know that $d_{1}=0$ and $d_{2} = 1$.

    Give a combinatorial argument that 
    \begin{equation*}
        d_{n} = (n-1)(d_{n-1} + d_{n-2}), \qquad \text{for } n \ge 2.
    \end{equation*}

    \hint{} For a derangement $σ$, 
    consider the integer $k$ with $σ(k) = 1$.
    Argue based on the number of choices for $k$ and then two different cases.
\end{frame}
 
\subsection{7.5 The Euler Totient Function}

\begin{frame}[t]
    \frametitle{The Euler $\phi$/totient function}
    
    For a positive integer n ≥ 2, let
    \begin{equation*}
        \phi(n) = |\{m \in \dsZ_{+} : m \le n, \gcd(m, n) = 1\}|.
    \end{equation*}
    
    \cake{} 
    What is $\phi(5), \phi(9)$?
    What about $\phi(p)$ when $p$ is a prime number?
\end{frame}

\begin{frame}[t]
    \frametitle{\sweat{} Warming up --- Count multipliers}

    $12$ has two prime factors $p_{1} = 2, p_{2} = 3$.

    How many in $[12]$ are multiples of $2$, $3$ and $2 \times 3$ respectively?

    \pause{}

    \begin{exampleblock}{Proposition 7.15}
        Let $n \ge 2, k \ge 1$, and let $p_1 , p_2 , \ldots , p_k$ 
        be distinct primes each of which divide $n$ evenly.
        Then the number of integers from $\{1, 2, \ldots, n\}$
        which are divisible by each of these k primes is
        \begin{equation*}
            \frac{n}{p_{1}p_{2}\ldots p_{k}}
        \end{equation*}
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Compute $\phi(n)$}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            $30$ has 3 \emph{distinct} prime factors $2, 3, 5$.

            Let $P_{1}, P_{2}, P_{3}$ be the properties of 
            being a multiple of $2, 3, 5$ respectively.

            Can you use inclusion-exclusion to compute $\phi(30)$?
        \end{column}
        \begin{column}{0.4\textwidth}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{venn-3.png}
    \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{A theorem for $\phi(n)$}

    \begin{block}{Theorem 7.14.}
    Let $n \ge 2$ be a positive integer and suppose that $n$ has $m$ distinct prime factors:
    $p_1 , p_2 , \ldots, p_m$. 
    Then
    \begin{equation*}
        \phi(n)= n \prod_{i=1}^{m} \frac{p_{i}-1}{p_{i}}
    \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 7.16}
    Let
    \begin{equation*}
        n 
        = 
        7290
        =
        2
        \times
        5
        \times
        3^{6}
    \end{equation*}

    \cake{} What is $\phi(n)$?
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-10.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 7.7: 19, 21, 23, 25, 27.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}


\end{document}
