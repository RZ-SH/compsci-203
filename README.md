# COMPSCI203 -- Discrete Mathematics for Computer Science

This repository contains materials which [I (Xing Shi Cai)](https://newptcai.gitlab.io)
prepared for the course 
COMPSCI203 (Discrete Mathematics for Computer Science)
of [Duke Kunshan University](https://dukekunshan.edu.cn/),
taught from 2022-08 till 2022-10.

You are free to use any material here.
