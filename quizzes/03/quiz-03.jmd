---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 03
author: "Instructor Xing Shi Cai"
date: 2022-09-16
fontsize: 12pt
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using Plots
using Random
using Distributions
using DataFrames
using SymPy

# Seeding

myseed = 1234;
Random.seed!(myseed);

# LaTeX
set_default(env=:raw)

function short_answer()
print(L"""
\smallskip{}
Answer: $\underline{\hspace{5cm}}$
""")
end;

function short_answer(ans)
print(L"""
\smallskip{}
Answer: $\underline{%$ans}$
""")
end;
```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} Honour Pledge}]
\large{}

$\text{\emoji{panda}}$
I will neither give nor receive aid on this assessment except the textbooks,
solutions manuals for the assignments.

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

# Inclusion-Exclusion (1pt)

```julia
up = rand(11:20)
low = rand(0:9)
total = 4*rand(low+1:up-1)
newtotal = total -4 * low + 4
newup = up - low + 1

ans = sum([(-1)^i*binomial(4, i) * binomial(newtotal - i* newup -1, 4-1) for i in 0:3])
```

You would like to give $`j total`$ :coin: to your four children so that
each of them have at least $`j low`$ and at most $`j up`$ :coin:.
In how many ways can you do that?

:bulb: The answer should be given in terms of binomial coefficients.

Solution:

This is equivalent to finding the integer solutions for
\begin{equation*}
    y_1 + y_2 + y_3 + y_4 
    = `j total` -4 \cdot `j low` + 4 
    = `j newtotal`
\end{equation*}
with $0 < y_1, y_2, y_3, y_4 \le `j up` - `j low` + 1 = `j up - low + 1`$.

So by inclusion-exclusion principle, the solution is

\begin{equation*}
    \begin{aligned}
    \binom{`j newtotal` - 1}{4-1}
    &
    - 
    \binom{4}{1} 
    \binom{`j newtotal` - `j newup` - 1}{4 - 1}
    + 
    \binom{4}{2} 
    \binom{`j newtotal` - 2 \cdot `j newup` - 1}{4 - 1}
    \\
    &
    - 
    \binom{4}{3} 
    \binom{`j newtotal` - 3 \cdot `j newup` - 1}{4 - 1}
    + 
    0
    =
    `j ans`
    \end{aligned}
\end{equation*}

Remark:

Using the following generating function,
```julia
x = Sym("x")
gf = sum([x^i for i in low:up])^4
ans = gf.expand().coeff(x^total);
print(latexify(gf, env=:eq));
```
it is easy to verify with a computer that the answer is indeed `j ans`.

# Generating functions (1pt)

Get the coefficient of $x^9$ in the following generating functions

##

\begin{equation*}
(x^{3} + x^{5} + x^{6} )(x^{4} + x^{5} + x^{7} )(1 + x^{5} + x^{10} + x^{15} +
\cdots)
\end{equation*}

```julia
x = Sym("x")
gf = (x^(3) + x^(5) + x^(6) )*(x^(4) + x^(5) + x^(7) )*(1 + x^(5) + x^(10) + x^(15))
ans = gf.expand().coeff(x^9);
short_answer(ans);
```
Remark:

There is only one way to get $x^9$, taking $x^5$, $x^4$ and $1$ from respectively
from eact factor.

##

\begin{equation*}
\frac{x^3}{1-5 x^3}
\end{equation*}

```julia
x = Sym("x")
gf = (x^3)/(1-5*x^3)
ans = gf.series(x, 0, 10).coeff(x^9);
short_answer(ans);
```

# Generating functions (1pt)

Get a closed formula of the coefficient of $x^n$ in

\begin{equation*}
\frac{1+x}{(1-x)^3}
\end{equation*}

*without* using a :laptop:.
Explain how you did it.

:bulb: You can still use a :laptop: to verify your answer if you have time.

Solution:

The answer is simply $(n+2)^2$.
To get this, note that
\begin{equation*}
\frac{1}{(1-x)^3}
=
\frac{1}{2} 
\frac{\mathrm d^2}{\mathrm d x^2}
\frac{1}{1-x}
=
\frac{1}{2} 
\sum_{n=0}^\infty n (n-1) x^{n-2}
=
\sum_{n=0}^\infty 
\frac{1}{2} 
(n+2) (n+1) x^{n}
\end{equation*}
and that
\begin{equation*}
\frac{x}{(1-x)^3}
=
\frac{x}{2} 
\frac{\mathrm d^2}{\mathrm d x^2}
\frac{x}{1-x}
=
\frac{x}{2} 
\sum_{n=0}^\infty n (n-1) x^{n-2}
=
\sum_{n=0}^\infty 
\frac{1}{2} 
(n+1) (n) x^{n}
\end{equation*}

```julia
x = Sym("x")
gf = (1+x)/((1-x)^3)
ans = gf.series(x, 0, 10);
```
\pagebreak{}
