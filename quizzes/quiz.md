---
title: Quiz Exam Procedure
subtitle: COMPSCI 203 Discrete Mathematics for Computer Science
colorlinks: true
urlcolor: Maroon
marings: 1.5in
...

# Quizzes

For each lecture on Thursdays from week 2 to week 7, 
we will spend the last few minutes on quizzes.

The problems in quizzes will be selected from the assignment of the previous week,
with some **modifications**.

💣 The quizzes are **open** book.
But you are only allowed to check the **two textbooks** and **assignment solutions**.

## How to take the quiz:

1. When the quiz starts, go to [GradeScope](https://www.gradescope.com/courses/365483), download the quiz PDF file.
2. If you have an iPad/Tablet, answer the questions directly on the PDF.
3. Otherwise, write on a piece of paper and scan it with your phone. (Ideally
   with a scanning app.)
5. You have in total 20 minutes to download, answer and upload.
6. Email me a copy of your answers if you have trouble uploading.
