---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 06
author: "Instructor Xing Shi Cai"
date: 2022-10-13
fontsize: 12pt
numbersections: true
colorlinks: true
urlcolor: Maroon
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using Random
using Graphs, GraphPlot, Compose, Cairo, Colors
using SimpleGraphs

# Convert between SimpleGraphs and Graphs
# include(normpath(pathof(SimpleGraphs), "../../extras", "graph_converter.jl"))

# Seeding
myseed = 1234;

# LaTeX
set_default(env=:raw)

function short_answer()
print(L"""
\smallskip{}
Answer: $\underline{\hspace{5cm}}$
""")
end;

function short_answer(ans)
print(L"""
\smallskip{}
Answer: $\underline{%$ans}$
""")
end;

function short_answer(ans::LaTeXString)
    short_answer(ans[2:end-1])
end;

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} Honour Pledge}]
\large{}

$\text{\emoji{panda}}$
I will neither give nor receive aid on this assessment except the textbooks,
solutions manuals for the assignments.

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

# Graph Colouring
 
Let $n_t$ be the number of vertices in $G_t$ from the Kelly and Kelly proof of 
Proposition 5.25.

##

What is $n_4$? Given an integer as the answer. You can use a calculator.

```julia; results = "hidden"
n3 = 5
ni =  3 * (n3 - 1) +1
n4 = ni + n3*binomial(ni, n3)
#short_answer()
```

Answer:

The size of $I$ is
\begin{equation*}
    |I| = 3 \times (n_3 - 1) + 1 = `j ni`
\end{equation*}

So
\begin{equation*}
    n_{4} 
    = 
    |I| + n_3 \binom{|I|}{n_3}
    =
    `j ni` + `j n3` \binom{`j ni`}{`j n3`}
    =
    `j n4`
    .
\end{equation*}

##

What is $n_5$? You can use a binomial coefficient in your answer.

```julia; results = "hidden"
ni =  4 * (n4 - 1) +1
#short_answer()
```

The size of $I$ is
\begin{equation*}
    |I| = 4 \times (n_4 - 1) + 1 = `j ni`
\end{equation*}

So
\begin{equation*}
    n_{5} 
    = 
    |I| + n_4 \binom{|I|}{n_4}
    =
    `j ni` + n_4 \binom{`j ni`}{`j n4`}
    .
\end{equation*}

# Hypercube graph

A hypercube graph of $n$-dimension $Q_n$
has the set of binary strings of length $n$
as its vertex set.
Two vertices in $Q_n$ are adjacent if and only if
there their corresponding binary strings differ at exactly one position.

$Q_1, Q_2$ and $Q_3$ are shown below.

```julia; results = "tex"

"Create a hypercube graph of n dimension"
function make_hypercube_graph(n)
    g = SimpleGraph(2^n)
    vnum = 2^n

    # Create labels
    nodelabel = String[]
    for i in 0:vnum-1
        s = last(bitstring(i), n)
        push!(nodelabel, s)
    end

    # Add edges
    for i in 0:vnum-2
        for j in i+1:vnum-1
            # Compare hamming distance
            s1 = nodelabel[i+1]
            s2 = nodelabel[j+1]
            dist = 0
            for k in 1:n
                if s1[k] != s2[k]
                    dist +=1
                end
            end
            if dist == 1
                add_edge!(g, i+1, j+1)
            end
        end
    end
        
    return g, nodelabel
end

function save_hypercube_graph(n, size=8, NODESIZE=0.2)
    g, nodelabel = make_hypercube_graph(n)
    g_path = tempname() * ".pdf"
    draw(PDF(g_path, (size)cm, (size)cm), 
        gplot(g, 
            NODESIZE=NODESIZE,
            NODELABELSIZE=6,
            nodelabeldist=0, 
            nodelabelangleoffset=π/4,
            nodelabel=nodelabel,
            nodestrokec=colorant"black",
            nodestrokelw=2,
            nodefillc=colorant"orange";
            ))
    return g_path
end

for n in 1:3
    g_path = save_hypercube_graph(n)

    println(L"![$Q_{%$n}$](%$g_path){width=\"150px\"}")
end
```

##

Draw $Q_4$

Answer:

There are many way to draw it.
Some nicer than others.
Just search [4-dimension hypercube](https://www.ecosia.org/images?q=4%20dimension%20hypercube).

One possibility ---

```julia; results = "tex"
Random.seed!(myseed+3)
g_path = save_hypercube_graph(4, 20, 0.1)
println(L"![$Q_4$](%$g_path){width=\"400\"}")
```

##

What is $\chi(Q_4)$?

```julia
short_answer(2)
```

##

Can you guess what is $\chi(Q_{10})$?
(:bulb: Look for a pattern.)

```julia
short_answer(2)
```

##

Prove your guess of $\chi(Q_{10})$ is correct.

Proof:

Sum the "1"'s in a nodes label.
If the result is even colour the node \textcolor{orange}{orange},
otherwise colour it \textcolor{Green}{green}.
There is no edges between nodes of the same colour,
because their label differs at most one position.

In fact, this argument works with $Q_n$ for all $n \in \mathbb{N}$.
